import { createMuiTheme } from "@material-ui/core/styles";
import { responsiveFontSizes } from "@material-ui/core";

const Theme = createMuiTheme({
  palette: {
    type: "dark",
    primary: {
      main: "#fff",
    },
    secondary: {
      main: "#ffdd59",
      contrastText: "#1E1F21",
    },
    background: {
      default: "#1E1F21",
      paper: "#131415",
    },
  },
  typography: {
    fontFamily: ["Manrope"],
    h1: {
      fontSize: 48,
      fontWeight: 700,
    },
    h2: {
      fontSize: 40,
      fontWeight: 700,
    },
    h3: {
      fontSize: 28,
      fontWeight: 700,
    },
    h4: {
      fontSize: 22,
      fontWeight: 700,
    },
    body1: {
      fontSize: 18,
      fontWeight: 500,
    },
  },
  breakpoints: {
    values: {
      xs: 0,
      sm: 720,
      md: 960,
      lg: 1280,
      xl: 1920,
    },
  },
});

export default responsiveFontSizes(Theme);
