import React from "react";
import Layout from "../components/Layout";
import NavBar from "../components/NavBar";
import Description from "../sections/Description";
import Hero from "../sections/Hero";
import Languages from "../sections/Languages";
import Footer from "../sections/Footer";

const IndexPage = () => {
  return (
    <Layout>
      <NavBar />
      <Hero />
      <Description />
      <Languages />
      <Footer />
    </Layout>
  );
};

export default IndexPage;
