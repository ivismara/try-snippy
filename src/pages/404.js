import * as React from "react";
import { Link } from "gatsby";
import Section from "../components/Section";
import SectionTitle from "../components/SectionTitle";
import { Typography } from "@material-ui/core";
import { ChevronLeft } from "react-feather";

// styles
const pageStyles = {
  color: "#232129",
  padding: "96px",
  fontFamily: "-apple-system, Roboto, sans-serif, serif",
};
const headingStyles = {
  marginTop: 0,
  marginBottom: 64,
  maxWidth: 320,
};

const paragraphStyles = {
  marginBottom: 48,
};
const codeStyles = {
  color: "#8A6534",
  padding: 4,
  backgroundColor: "#FFF4DB",
  fontSize: "1.25rem",
  borderRadius: 4,
};

// markup
const NotFoundPage = () => {
  return (
    <Section variant="primary">
      <div style={{ textAlign: "center" }}>
        <SectionTitle>Page not found 😔</SectionTitle>
        <div style={{ display: "flex", justifyContent: "center" }}>
          <Link
            to="/"
            style={{
              color: "inherit",
              textDecoration: "none",
              display: "flex",
              alignItems: "center",
              marginTop: 48,
            }}
          >
            <ChevronLeft size={28} style={{ marginRight: 18 }} />
            <Typography variant="h4" component="p">
              Go back to the homepage
            </Typography>
          </Link>
        </div>
      </div>
    </Section>
  );
};

export default NotFoundPage;
