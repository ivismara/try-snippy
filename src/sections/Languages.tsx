import React from "react";
import Section from "../components/Section";
import SectionTitle from "../components/SectionTitle";
import { makeStyles, Typography } from "@material-ui/core";
import "devicon/devicon.min.css";

const useStyles = makeStyles((theme) => ({
  root: {
    textAlign: "center",
    [theme.breakpoints.down("md")]: {
      marginBottom: 48,
    },
  },
  iconContainer: {
    marginTop: 48,
    position: "relative",
    maxWidth: 500,
    width: "100%",
    margin: "0 auto",
    display: "flex",
    justifyContent: "space-between",
    marginBottom: 24,
  },
  icon: {
    opacity: 0.6,
    fontSize: 42,
    [theme.breakpoints.down("md")]: {
      fontSize: 28,
    },
  },
}));

const Languages = () => {
  const classes = useStyles();
  return (
    <Section variant="primary">
      <div className={classes.root}>
        <SectionTitle>Multi-language support 👨‍💻👩‍💻</SectionTitle>

        <Typography variant="body1" component="p">
          Write code in your favourite programming language!
        </Typography>
        <div className={classes.iconContainer}>
          <i className={`devicon-typescript-plain ${classes.icon}`}></i>
          <i className={`devicon-python-plain ${classes.icon}`}></i>
          <i className={`devicon-go-plain ${classes.icon}`}></i>
          <i className={`devicon-java-plain ${classes.icon}`}></i>
          <i className={`devicon-javascript-plain ${classes.icon}`}></i>
          <i className={`devicon-css3-plain ${classes.icon}`}></i>
          <i className={`devicon-sass-plain ${classes.icon}`}></i>
        </div>
        <Typography variant="body2" component="p">
          And many others...
        </Typography>
      </div>
    </Section>
  );
};

export default Languages;
