import React from "react";
import Section from "../components/Section";
import { Avatar, Grid, makeStyles, Typography } from "@material-ui/core";
import { graphql, Link, useStaticQuery } from "gatsby";
import { Twitter } from "react-feather";

const useStyles = makeStyles((theme) => ({
  root: {
    [theme.breakpoints.down("md")]: {
      paddingTop: 32,
      paddingBottom: 32,
    },
  },
  link: {
    textDecoration: "none",
    color: "inherit",
    cursor: "pointer",
    borderBottomStyle: "solid",
    borderBottomWidth: 1,
    borderBottomColor: theme.palette.secondary.main,
  },
  title: {
    marginBottom: 24,
    [theme.breakpoints.down("md")]: {
      marginBottom: 12,
    },
  },
  lastLine: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: theme.palette.background.paper,
    padding: "10px 0",
  },
  buyMeACoffee: {
    textAlign: "right",
  },
  meContainer: {
    display: "flex",
    alignItems: "center",
  },
  icon: {
    color: "inherit",
    height: 18,
  },
}));

const Footer = () => {
  const classes = useStyles();

  const image = useStaticQuery(graphql`
    query {
      me: file(relativePath: { eq: "me.png" }) {
        childImageSharp {
          fluid(maxWidth: 400, quality: 100) {
            srcWebp
          }
        }
      }
    }
  `);

  return (
    <>
      <Section variant="dark">
        <Grid container spacing={5} className={classes.root}>
          <Grid item md={4} xs={12}>
            <Typography variant="h3" component="h5" className={classes.title}>
              Developed by
            </Typography>
            <div className={classes.meContainer}>
              <Avatar src={image.me.childImageSharp.fluid.srcWebp} />
              <Typography
                variant={"body1"}
                component="p"
                style={{ marginLeft: 24, marginRight: 24 }}
              >
                Ilyich Vismara
              </Typography>
              <a
                href="https://twitter.com/vismarailyich"
                target="_blank"
                className={classes.icon}
              >
                <Twitter size={18} />
              </a>
            </div>
          </Grid>
          <Grid item md={4} xs={12}>
            <Typography variant="h3" component="h5" className={classes.title}>
              Suggestions?
            </Typography>
            <Typography variant={"body1"} component="p">
              Reach us on{" "}
              <a
                className={classes.link}
                href="https://twitter.com/TrySnippy"
                target="_blank"
              >
                Twitter
              </a>
              !
            </Typography>
          </Grid>

          <Grid item md={4} xs={12} className={classes.buyMeACoffee}>
            <a href="https://www.buymeacoffee.com/ivismara" target="_blank">
              <img
                src="https://cdn.buymeacoffee.com/buttons/v2/default-yellow.png"
                alt="Buy Me A Coffee"
                style={{ height: 35 }}
              />
            </a>
          </Grid>
        </Grid>
      </Section>
      <Grid container>
        <Grid item className={classes.lastLine} xs={12}>
          <Typography variant="body2" component="p" style={{ opacity: 0.6 }}>
            @{new Date().getFullYear()} -{" "}
            <Link
              to="/privacy-policy"
              style={{ color: "inherit", textDecoration: "none" }}
            >
              Privacy Policy
            </Link>
          </Typography>
        </Grid>
      </Grid>
    </>
  );
};

export default Footer;
