import React from "react";
import Motto from "../components/Motto";
import ChromeBadge from "../components/ChromeBadge";
import Section from "../components/Section";
import { graphql, useStaticQuery } from "gatsby";
import { Grid, makeStyles } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  image1: {
    position: "absolute",
    bottom: -100,
    right: 250,
    [theme.breakpoints.down("md")]: {
      right: 116,
      width: 200,
    },
    [theme.breakpoints.down("sm")]: {
      right: 100,
    },
  },
  image2: {
    position: "absolute",
    bottom: -50,
    right: 80,
    [theme.breakpoints.down("md")]: {
      right: 32,
      width: 200,
    },
    [theme.breakpoints.down("sm")]: {
      right: 16,
    },
  },
  fixedHeight: {
    [theme.breakpoints.down("md")]: {
      height: 200,
      width: "100%",
      marginTop: 56,
    },
  },
}));

const Hero = () => {
  const classes = useStyles();
  const image = useStaticQuery(graphql`
    query {
      screen1: file(relativePath: { eq: "screen-1.png" }) {
        childImageSharp {
          fluid(maxWidth: 300, quality: 100) {
            srcWebp
          }
        }
      }
      screen2: file(relativePath: { eq: "screen-2.png" }) {
        childImageSharp {
          fluid(maxWidth: 300, quality: 100) {
            srcWebp
          }
        }
      }
    }
  `);

  return (
    <Section variant="secondary">
      <Grid container>
        <Grid item sm={6} lg={8}>
          <Motto />
          <ChromeBadge />
        </Grid>
        <Grid item sm={6} lg={4} className={classes.fixedHeight}>
          <img
            alt="screen-1"
            src={image.screen1.childImageSharp.fluid.srcWebp}
            className={classes.image1}
          />
          <img
            alt="screen-2"
            src={image.screen2.childImageSharp.fluid.srcWebp}
            className={classes.image2}
          />
        </Grid>
      </Grid>
    </Section>
  );
};

export default Hero;
