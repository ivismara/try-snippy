import React from "react";
import { Grid, makeStyles, Typography } from "@material-ui/core";
import SectionTitle from "../components/SectionTitle";
import { graphql, useStaticQuery } from "gatsby";
import Section from "../components/Section";
// @ts-ignore
import Sprinkles from "../images/sprinkles.svg";

const useStyles = makeStyles((theme) => ({
  root: {
    paddingTop: 60,
    [theme.breakpoints.down("md")]: {
      paddingTop: 48,
      marginBottom: 24,
    },
  },
  container: {
    position: "relative",
    maxWidth: 600,
    width: "100%",
    margin: "0 auto",
  },
  box: {
    background: theme.palette.background.paper,
    padding: "24px 48px",
    borderRadius: 12,
    position: "relative",
    width: "100%",
    textAlign: "center",
    [theme.breakpoints.down("md")]: {
      padding: 0,
      backgroundColor: theme.palette.background.default,
    },
  },
  image: {
    maxWidth: 400,
    width: "100%",
  },
  sprinkle1: {
    position: "absolute",
    left: -50,
    top: -50,
    [theme.breakpoints.down("md")]: {
      display: "none",
    },
  },
  sprinkle2: {
    position: "absolute",
    bottom: -45,
    right: -45,
    transform: "rotate(180deg)",
    [theme.breakpoints.down("md")]: {
      display: "none",
    },
  },
}));

const Description = () => {
  const classes = useStyles();

  const image = useStaticQuery(graphql`
    query {
      screen: file(relativePath: { eq: "folder-screen.png" }) {
        childImageSharp {
          fluid(maxWidth: 400, quality: 100) {
            srcWebp
          }
        }
      }
    }
  `);

  return (
    <Section variant="primary">
      <Grid container spacing={5} className={classes.root}>
        <Grid item xs={12}>
          <div className={classes.container}>
            <img
              src={Sprinkles}
              alt={"sprinkles-1"}
              className={classes.sprinkle1}
            />
            <img
              src={Sprinkles}
              alt={"sprinkles-2"}
              className={classes.sprinkle2}
            />
            <div className={classes.box}>
              <SectionTitle>Improve your workflow 🚀</SectionTitle>
              <Typography variant="body1" component="p">
                Snippets will always be at your fingertips while browsing the
                web.
              </Typography>
              <Typography
                variant="body1"
                component="p"
                style={{ marginBottom: 24 }}
              >
                Organize them in folder and boost your productivity!
              </Typography>
              <img
                className={classes.image}
                src={image.screen.childImageSharp.fluid.srcWebp}
                alt="folder structure"
              />
            </div>
          </div>
        </Grid>
      </Grid>
    </Section>
  );
};

export default Description;
