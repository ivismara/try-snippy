import React from "react";
import { makeStyles } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  root: {
    width: 200,
    [theme.breakpoints.down("md")]: {
      width: 130,
    },
  },
}));

const PHBadge = () => {
  const classes = useStyles();
  return (
    <a
      href="https://www.producthunt.com/posts/snippy-3?utm_source=badge-featured&utm_medium=badge&utm_souce=badge-snippy-3"
      target="_blank"
    >
      <img
        src="https://api.producthunt.com/widgets/embed-image/v1/featured.svg?post_id=286277&theme=light"
        alt="Snippy - Manage your snippets in Chrome | Product Hunt"
        className={classes.root}
      />
    </a>
  );
};

export default PHBadge;
