import React from "react";
import SEO from "./Seo";

const Layout = ({ children }) => {
  return (
    <>
      <SEO />
      {children}
    </>
  );
};

export default Layout;
