import React from "react";
import { makeStyles, Typography } from "@material-ui/core";

interface SectionTitleProps {
  children: string;
}

const useStyles = makeStyles((theme) => ({
  root: {
    borderBottomColor: theme.palette.secondary.main,
    borderBottomWidth: 3,
    borderBottomStyle: "solid",
    paddingBottom: 8,
    marginBottom: 16,
    display: "inline-block",
  },
}));

const SectionTitle = ({ children }: SectionTitleProps) => {
  const classes = useStyles();
  return (
    <Typography variant="h3" component="h3" className={classes.root}>
      {children}
    </Typography>
  );
};

export default SectionTitle;
