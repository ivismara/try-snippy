import React from "react";
// @ts-ignore
import GetItOnChrome from "../images/chrome.png";
import { makeStyles } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  root: {
    borderRadius: 12,
    backgroundColor: "white",
    width: 250,
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    marginTop: 64,
    marginBottom: 20,
    [theme.breakpoints.down("md")]: {
      width: 180,
      borderRadius: 8,
      marginTop: 32,
    },
  },
  image: {
    width: 250,
    [theme.breakpoints.down("md")]: {
      width: 180,
    },
  },
}));

const ChromeBadge = () => {
  const classes = useStyles();
  return (
    <a
      href="https://chrome.google.com/webstore/detail/snippy/hloafekbnjflohjbafgbhejbhdloaibb"
      target="_blank"
    >
      <div className={classes.root}>
        <img
          src={GetItOnChrome}
          className={classes.image}
          alt="Get it on Chrome"
        />
      </div>
    </a>
  );
};

export default ChromeBadge;
