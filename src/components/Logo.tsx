import { makeStyles, Typography } from "@material-ui/core";
import React from "react";
import { Code } from "react-feather";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    color: theme.palette.secondary.contrastText,
  },
  image: {
    padding: 6,
    borderRadius: 8,
    height: 40,
    width: 40,
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    color: theme.palette.secondary.contrastText,
    marginRight: 12,
  },
}));

export default function Logo() {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <div className={classes.image}>
        <Code size={80} strokeWidth={2} />
      </div>
      <Typography variant="h4" component="h1" color="inherit">
        Snippy
      </Typography>
    </div>
  );
}
