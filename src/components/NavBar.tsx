import React from "react";
import { makeStyles } from "@material-ui/core";
import Logo from "./Logo";
import PHBadge from "./PHBadge";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    padding: "20px 60px",
    background: theme.palette.secondary.main,
    [theme.breakpoints.down("md")]: {
      padding: "16px 32px",
    },

    [theme.breakpoints.down("sm")]: {
      padding: 16,
    },
  },
}));

const NavBar = () => {
  const classes = useStyles();
  return (
    <nav className={classes.root}>
      <Logo />
      <PHBadge />
    </nav>
  );
};

export default NavBar;
