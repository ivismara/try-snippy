import React, { useEffect, useState } from "react";
import { makeStyles, Typography } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  root: {
    marginTop: 20,
  },
  text: {
    color: theme.palette.secondary.contrastText,
  },
  changingText: {
    fontWeight: 700,
    backgroundColor: theme.palette.background.default,
    display: "inline-block",
    padding: 8,
    color: theme.palette.secondary.main,
    marginBottom: 16,
  },
}));

const Motto = () => {
  const availableWords = ["Organize", "Save", "Manage", "Bookmark", "Boost"];
  const [activeWord, setActiveWord] = useState(0);
  const classes = useStyles();

  useEffect(() => {
    const interval = setInterval(() => {
      setActiveWord(Math.floor(Math.random() * availableWords.length - 1) + 1);
    }, 800);
    return () => clearInterval(interval);
  }, []);

  return (
    <div className={classes.root}>
      <Typography variant="h1" component="h3" className={classes.text}>
        <div className={classes.changingText}>{availableWords[activeWord]}</div>
        <div>Your code directly in Chrome!</div>
      </Typography>
    </div>
  );
};

export default Motto;
