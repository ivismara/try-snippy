import React from "react";
import { makeStyles, useTheme } from "@material-ui/core";

interface SectionProps {
  children: any;
  variant: "primary" | "secondary" | "dark";
}

const useStyles = makeStyles((theme) => ({
  root: {
    padding: "60px 80px",
    position: "relative",
    overflow: "hidden",
    [theme.breakpoints.down("md")]: {
      padding: "16px 32px",
    },
    [theme.breakpoints.down("sm")]: {
      padding: 16,
    },
  },
}));

const Section = ({ children, variant }: SectionProps) => {
  const classes = useStyles();
  const theme = useTheme();

  const getBackgroundColor = () => {
    switch (variant) {
      case "primary":
        return theme.palette.background.default;
      case "secondary":
        return theme.palette.secondary.main;
      default:
        return theme.palette.background.paper;
    }
  };

  return (
    <div
      className={classes.root}
      style={{
        backgroundColor: getBackgroundColor(),
      }}
    >
      {children}
    </div>
  );
};

export default Section;
