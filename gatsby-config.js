module.exports = {
  pathPrefix: `/`,
  siteMetadata: {
    title: "Snippy",
    description: "Organize your snippets directly in Chrome",
    url: "https://trysnippy.com",
    siteUrl: "https://trysnippy.com",
    image: "/images/meta.png",
    twitterUsername: "@TrySnippy",
  },
  plugins: [
    {
      resolve: `gatsby-plugin-google-gtag`,
      options: {
        trackingIds: ["G-M8X43VJXWP"],
      },
    },
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: "Snippy",
        short_name: "Snippy",
        start_url: "/",
        display: "standalone",
        icon: "src/images/icon.png",
      },
    },
    "gatsby-plugin-react-helmet",
    "gatsby-plugin-sharp",
    "gatsby-plugin-sitemap",
    "gatsby-transformer-sharp",
    {
      resolve: "gatsby-source-filesystem",
      options: {
        name: "images",
        path: "./src/images/",
      },
      __key: "images",
    },
    {
      resolve: `gatsby-theme-material-ui`,
      options: {
        webFontsConfig: {
          fonts: {
            google: [
              {
                family: `Manrope`,
                variants: [`300`, `400`, `500`, `700`],
              },
            ],
          },
        },
      },
    },
  ],
};
